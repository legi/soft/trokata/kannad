# Kannad - Send message on all console

[kannad](https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/kannad) writes a message to all users like the command `wall`.
On modern terminal like under GNOME or XFCE (`gnome-terminal`...),
the `wall` command does not work.
`kannad` tries to be as compatible as possible with the `wall` command options.
The message will be read from `STDIN` if the file name is empty or `-`.

**Kannad** simply means "message" in **Breton language**.

An easy way to use the latest version of [kannad]([https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/kannad/-/raw/master/kannad?inline=false)
without getting the whole repository is :
```bash
wget https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/kannad/-/raw/master/kannad?inline=false -O kannad
chmod u+x ./kannad
```

For `kannad` usage please read the fantastic [manual](https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/kannad/).
```bash
man kannad
```

## Usage

```bash
kannad [--nobanner|-n] [--group|-g group] [--help|-h] [--version|-V] [message|file]

# Simple message
kannad "My message"

# Via pipe, without banner and only on adm tty
echo "My message" | kannad -n -g adm
```


## Repository

### Source

The whole code is distributed under **free license**.
The script in `bash` is under GPL version 2 or more recent (http://www.gnu.org/licenses/gpl.html).
All sources are available on the Grenoble campus forge:
https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/kannad.
The source code is versionned with git (GitLab), it is very easy to keep in sync :

 * clone the sources
   ```bash
   git clone ttps://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/kannad
   ```

 * updates afterwards
   ```bash
   git pull
   ```


### Download

Up-to-date Debian packages are available at https://legi.gricad-pages.univ-grenoble-alpes.fr/soft/trokata/kannad/download.

Note that the Debian packages are very simple and certainly do not meet all the Debian Policy requirements.
They are however functional and in production at LEGI.

RPM package maintainer wanted !
